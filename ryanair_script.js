/*
    Version: 1.0
    Author: Sean Hayes
    This script is used to extract data from the Ryanair (ryanair.ie) flight
    itinerary/summary page after a specific flight (or set of return flights)
    has been selected.
    It transforms the data to a custom data model and posts the JSON payload to 
    api.capturedata.com
    Note: the entire script can be posted to the browser console for testing
*/


scrapeForFlightDetails().then((flights) => {
    if (flights) {
        console.log(JSON.stringify(flights));
        postToEndpoint(JSON.stringify(flights));
    } else {
        console.log("No flight details extracted");
    }
})

/*
    Holder method for determining flight type (one-way/return) and invoking data extraction
    and transformation
*/
var scrapeForFlightDetails = async () => {
    var flights = [];
    if (window.document.getElementsByClassName('flights-table-wrapper').length === 0) {
        console.log('No flight info');
        return null;
    } else if (window.document.getElementsByClassName('flights-table-wrapper').length === 1) {
        var outboundFlight = extractFlightDetails(0);
        flights.push(outboundFlight);
        console.log('Only outbound flight');
    } else if (window.document.getElementsByClassName('flights-table-wrapper').length === 2) {
        var outboundFlight = populateFlightDetails(0);
        flights.push(outboundFlight);
        var returnFlight = populateFlightDetails(1);
        flights.push(returnFlight);
        console.log('Return flights');
    } 

    return {
        flights
    }
}

/*
    This method populates and returns an instance of flight details for a specific flight
*/
function populateFlightDetails(index) {
    var flightDetails = {
        timestamp: new Date(),
        airline: 'Ryanair',
        flightNo: extractTextFromHtmlClass('flight-number', index),
        type: index === 0 ? "outbound" : "return",
        date: extractTextFromHtmlClass('flight-header__details-time', index),
        origin: extractTextFromHtmlClass('cities__departure', index),
        destination: extractTextFromHtmlClass('cities__destination', index),
        departureTime: extractTextFromHtmlClass('start-time', index),
        arrivalTime: extractTextFromHtmlClass('end-time', index),
        price: extractTextFromHtmlClass('flights-table-price__price', index)
    }
    return flightDetails;
}

/*
    This method extracts the text from the specified HTML class based on it's clasa name
    and index in the array returned from windows.document.getElementsByClassName()
*/
function extractTextFromHtmlClass(className, index) {
    if (window.document.getElementsByClassName(className)[index]) {
        return window.document.getElementsByClassName(className)[index].textContent;
    } else return "";
}

/*
    This method posts the JSON payload containing flight info to the api.capturedata.com
    endpoint.
*/
function postToEndpoint(payload) {
    var request = new XMLHttpRequest();
    var endpoint = "https://api.capturedata.com";

    request.open('POST', endpoint);
    request.setRequestHeader('Content-type', 'application/json');
    request.setRequestHeader('Access-Control-Allow-Methods', 'POST');
    request.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type');
    request.send(payload);     
}