# This script reads in a CSV file called input_data.csv from the same directory
# It parses each row in the file and transforms each row into a JSON object
# It then posts the JSON payload to http://api.capturedata.ie and writes
# each blob of JSON to a file called output_json.json 

import csv
import requests
import json

endpoint = 'http://api.capturedata.ie'

with open('input_data.csv', 'rb') as csvfile:
    reader = csv.DictReader(csvfile, fieldnames=('firstName','surname','passportNo','dob','flightNo','address','country'))
    for row in reader:
        payload = json.dumps(row)
	
        headers = {'Content-type': 'application/json'}
        try:
            response = requests.post(endpoint, data=payload, headers=headers)
        except:
            print("failed to post to endpoint")
		
        output = open('output_json.json', 'a')  
        output.write(payload)  
